#!/bin/python

import os
import sys
import copy
from memoization import cached


def from_this_directory():
    os.chdir(os.path.dirname(sys.argv[0]))


from_this_directory()

os.chdir("/home/volker/Sync/ila_parent/openfoam-debug/applications/test/tetTetOverlap")


gooddir = "/home/volker/Sync/ila_parent/openfoam-debug/builddir/"

good = "c++ -o /home/volker/OpenFOAM/volker-6/platforms/linux64GccDPInt32Opt/bin/Test-tetTetOverlap applications/test/tetTetOverlap/Test-tetTetOverlap.p/Test-tetTetOverlap.C.o -fsanitize=undefined -Wl,--allow-shlib-undefined -Wl,--add-needed -pthread -Wl,-rpath,$ORIGIN/../../../src/meshTools:$ORIGIN/../../../src/triSurface:$ORIGIN/../../../src/fileFormats:$ORIGIN/../../../src/surfMesh:$ORIGIN/../../../src/OpenFOAM:$ORIGIN/../../../src/OSspecific/POSIX:$ORIGIN/../../../src/Pstream/dummy -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/meshTools -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/triSurface -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/fileFormats -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/surfMesh -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/OpenFOAM -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/OSspecific/POSIX -Wl,-rpath-link,/home/volker/Sync/ila_parent/openfoam-debug/builddir/src/Pstream/dummy -Wl,--start-group src/meshTools/libmeshTools.so src/OpenFOAM/libOpenFOAM.so -lm -ldl -Wl,--end-group"

bad = "g++ -std=c++17 -m64 -fsanitize=undefined -Dlinux64 -DWM_ARCH_OPTION=64 -DWM_DP -DWM_LABEL_SIZE=32  -O0 -fdefault-inline -ggdb3 -DFULLDEBUG -DNoRepository -ftemplate-depth-100 -I/home/volker/Sync/ila_parent/openfoam-debug/src/meshTools/lnInclude -IlnInclude -I. -I/home/volker/Sync/ila_parent/openfoam-debug/src/OpenFOAM/lnInclude -I/home/volker/Sync/ila_parent/openfoam-debug/src/OSspecific/POSIX/lnInclude   -fPIC -Xlinker --add-needed -Xlinker --no-as-needed /home/volker/Sync/ila_parent/openfoam-debug/builddir/applications/test/tetTetOverlap/Test-tetTetOverlap.p/Test-tetTetOverlap.C.o -L/home/volker/Sync/ila_parent/openfoam-debug/platforms/linux64GccDPInt32Opt/lib \
    -lmeshTools -lOpenFOAM -ldl  \
    -ggdb3 -DFULLDEBUG -lm -o /home/volker/OpenFOAM/volker-6/platforms/linux64GccDPInt32Opt/bin/Test-tetTetOverlap"

additional_steps = "/home/volker/OpenFOAM/volker-6/platforms/linux64GccDPInt32Opt/bin/Test-tetTetOverlap"

both = ""


class CompArg:
    def __init__(self, arg):
        self.typ = "flag"
        if arg.startswith("-o "):
            self.typ = "output"
        elif (
            arg.endswith(".cpp")
            or arg.endswith(".c")
            or arg.endswith(".C")
            or arg.endswith(".o")
            or arg.endswith(".so")
        ):
            self.typ = "input"
        self.str = arg

    def __str__(self):
        return self.str

    def __repr__(self):
        return self.str

    pass


def strings_to_comp_args(sar):
    ret = []
    part = None
    for arg in sar:
        if arg in ["-o", "-Xlinker", "-MF", "-MT", "-MQ"]:
            part = arg
        elif part is None:
            ret.append(CompArg(arg))
        else:
            ret.append(CompArg(part + " " + arg))
            part = None
    return ret


def space_joiner(args):
    ret = " "
    for el in args:
        ret += str(el) + " "
    return ret


@cached
def works(args):
    cmd = "g++ " + both + space_joiner(args) + " &> /dev/null"
    # print("g++ " + both + space_joiner(args))
    if additional_steps is None:
        return os.system(cmd) == 0
    else:
        return os.system(cmd + " && " + additional_steps + " &> /dev/null") == 0


def make_absolute(arg, wdir):
    assert wdir[-1] == "/"  # todo: better error message
    if arg.str.startswith("-I") and not arg.str.startswith("-I/"):
        return CompArg("-I" + wdir + arg.str[2:])
    elif arg.str.startswith("-iquote") and not arg.str.startswith("-iquote/"):
        return CompArg("-iquote" + wdir + arg.str[7:])
    elif arg.typ == "input" and not arg.str.startswith("/"):
        return CompArg(wdir + arg.str)
    else:
        return arg


def remove_unnecessary(goodargs):
    assert works(goodargs)
    i = 0
    while i < len(goodargs):
        temp = copy.deepcopy(goodargs)
        if not temp[i].typ == "input":
            del temp[i]
            if works(temp):
                goodargs = copy.deepcopy(temp)
                i -= 1
        i += 1
    return goodargs


assert (
    '"' not in bad and "'" not in bad and "\\" not in bad
)  # todo: better error message
assert (
    '"' not in good and "'" not in good and "\\" not in good
)  # todo: better error message
assert (
    '"' not in both and "'" not in both and "\\" not in both
)  # todo: better error message
assert gooddir is None or " " not in gooddir

badargs = strings_to_comp_args(bad.split(" ")[1:])
goodargs = strings_to_comp_args(good.split(" ")[1:])
# goodargs = list(filter(lambda arg: arg.typ != "output", goodargs))
# badargs = list(filter(lambda arg: arg.typ != "output", badargs)) #todo this script might create random a.out files
if gooddir is not None:
    goodargs = [make_absolute(arg, gooddir) for arg in goodargs]
goodargs = remove_unnecessary(goodargs)
assert works(goodargs)
assert not works(badargs)

print(
    "the following works and is (maybe) shorter:",
    "g++ " + both + space_joiner(goodargs),
)
# def make_similar():
# 	print("c++" + space_joiner(goodargs) + both)

# make_similar()


def bisecter(checkfunc, left, right):
    assert checkfunc(left)
    assert not checkfunc(right)
    while right - left > 1:
        mid = int((right + left) / 2)
        if checkfunc(mid):
            left = mid
        else:
            right = mid
    return left, right


def checkfunc_1(i):
    return works(goodargs[i:] + badargs + goodargs[:i])


def checkfunc_2(i, important_args):
    return works(badargs[i:] + [important_arg] + badargs[:i])


badflags = list(filter(lambda arg: arg.typ == "flag", badargs))
goodflags = list(filter(lambda arg: arg.typ == "flag", goodargs))
# goodinput = list(filter(lambda arg: arg.typ == "input", goodargs))
if works(goodflags + badargs) and not works(badargs + goodflags):
    left, right = bisecter(
        lambda i: works(goodflags[i:] + badargs + goodflags[:i]), 0, len(goodflags)
    )
    important_arg = goodflags[left]
    print(important_arg)
    print(space_joiner(goodargs[left:] + badargs + goodflags[:left]))
    print(space_joiner(goodargs[right:] + badargs + goodflags[:right]))
    # alwaysar = []
    # def checkfunc(j):
    # 	leftargs = []
    # 	rightargs = []
    # 	for i in range(len(goodargs)):
    # 		if i >= j:
    # 			leftargs.append(goodargs[i])
    # 		else:
    # 			rightargs.append(goodargs[i])
    # 	return works(leftargs + badargs + rightargs)

    # left, right = bisecter(checkfunc, 0, len(goodargs))
    # alwaysar.append(goodargs[left])
    # #del bisectar[left]
    left, right = bisecter(lambda i: not checkfunc_2(i, important_arg), 0, len(badargs))
    print(important_arg, badargs[left])
elif works(badargs + goodflags) and not works(badargs):
    left, right = bisecter(lambda i: works(badargs + goodflags[i:]), 0, len(goodflags))
    print("it seems like you are (among other flags) missing the flag")
    print(goodflags[left])
elif not works(goodargs + badflags):
    left, right = bisecter(lambda i: works(goodargs + badflags[:i]), 0, len(badflags))
    print("the following argument is a bad suffix:", badflags[left])
elif not works(badflags + goodargs):
    left, right = bisecter(lambda i: works(badflags[:i] + goodargs), 0, len(badflags))
    important = badflags[left]
    print("the following argument is a bad prefix:", important)
    if works(goodargs + [important]):
        left, right = bisecter(
            lambda i: works(goodargs[i:] + [important] + goodargs[:i]), 0, len(goodargs)
        )
        print("this argument needs to be after", goodargs[left])
elif (
    works(goodargs + badflags)
    and works(badflags + goodargs)
    and not works(goodflags + badargs)
    and not works(badargs + goodflags)
):
    print("Difference in output or input flags")
    print(list(filter(lambda arg: arg.typ == "input", badargs)))
    print(list(filter(lambda arg: arg.typ == "input", goodargs)))

works(badargs + goodargs)
